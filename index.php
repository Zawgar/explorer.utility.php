<?php require_once("controlador.php"); ?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--JQuery-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!--Bootstrap-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!--Font-Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

    <!--ValidationEngine-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-es.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css" />
  </head>
  <body>

    <!-- CONTENIDO -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div id="menu"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div id="listadoDirectorios"></div>
        </div>
        <div class="col-md-9">
          <div id="listadoContenidos"></div>
        </div>
      </div>
    </div>

    <!-- Modal Notificaciones -->
    <div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body" id="modal_popup_texto" style="text-align:center;">
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Renombrar -->
    <div class="modal fade" id="modale_renombrar" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Renombrar</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" name="url" value="">
            <input type="hidden" name="original" value="">
            <div class="form-group">
              <label>Nombre</label>
              <input type="text" name="nombre" class="form-control validate[required]" value="">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary renombrarAceptar">Aceptar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Eliminar -->
    <div class="modal fade" id="modale_eliminar" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Eliminar</h4>
          </div>
          <div class="modal-body">
            ¿Esta seguro que desea eliminar?
          </div>
          <div class="modal-footer">
            <input type="hidden" name="url" value="">
            <input type="hidden" name="tipo" value="">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-danger eliminarAceptar">Eliminar</button>
          </div>
        </div>
      </div>
    </div>

    <script>
      var dirRaiz='<?php echo $config["directorioRaiz"]; ?>';
      var listadoDirectoriosData='<?php echo json_encode($listadoDirectorios); ?>';
    </script>
    <link rel="stylesheet" href="general.css" />
    <script src="general.js"></script>

  </body>
</html>