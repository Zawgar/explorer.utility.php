$(document).ready(function(){
  // ******************************************************************************************************
  // Configuración ****************************************************************************************
  // ******************************************************************************************************
  var carpetaActual=dirRaiz; // mantiene la direccion de la carpeta actual

  var menu=$("#menu");
  var listadoDirectorios=$("#listadoDirectorios");
  var listadoContenidos=$("#listadoContenidos");


  // ******************************************************************************************************
  // Listado de Directorios *******************************************************************************
  // ******************************************************************************************************
  function cargarDirectorios(){
    var request = $.ajax({
      url: document.location.href,
      type: "POST",
      data: {
        accion: "cargarDirectorios",
      }
    });
    request.done(function(data){
      listadoDirectorios.html("");
      maquetarDirectorios(data, 0, "");
      estadosDirectorios();
    });
    request.fail(function(){
    });
    request.always(function(){
    });
  }

  function maquetarDirectorios(data, nivel, padre){
    $.each(data, function(index, value){
      dibujarCarpeta(value.nombre, value.href, value.cerrado, nivel, padre);
      if(value.subcarpetas.length){
        maquetarDirectorios(value.subcarpetas, (nivel+1), value.href);
      }
    });
  }

  function dibujarCarpeta(nombre, href, cerrado, nivel, padre){
    if(cerrado){
      estado="cerrado";
      icono="<span class='icon expand-icon fa fa-folder-o'></span>";
    }else{
      estado="abierto";
      icono="<span class='icon expand-icon fa fa-folder-open-o'></span>";
    }

    espacios="";
    for(x = 0; x < nivel; x++){
      espacios+=" - ";
    }

    directorio="<a data-url='"+href+"' data-estado='"+estado+"' data-padre='"+padre+"'>"+espacios+icono+" "+nombre+"</a>";
    listadoDirectorios.append(directorio);
  }

  function estadosDirectorios(){
    $("#listadoDirectorios a").each(function(){
      if($(this).data("estado")=="cerrado"){
        $("#listadoDirectorios a[data-padre='"+$(this).data("url")+"']").hide(0);
      }
    });
  }

  listadoDirectorios.on("click", "a", function(){
    dirRaiz=$(this).data("url");
    cargarContenidor(dirRaiz);
  });

  listadoDirectorios.on("dblclick", "a", function(){
    var actual=$(this).data("url");
    //$("#listadoDirectorios a[data-padre='"+actual+"']").slideToggle(0);
    /*
    $.each($("#listadoDirectorios a"), function(index, value){
      aparicion=$(this).data("padre").search(actual);
      if(aparicion != -1){
        $(this).slideUp(0);
      }
    });
    */
  });


  // ******************************************************************************************************
  // Listado de Contenidos ********************************************************************************
  // ******************************************************************************************************
  function cargarContenidor(directorio){
    var request=$.ajax({
      url: document.location.href,
      type: "POST",
      data: {
        accion: "listarContenidoCarpeta",
        dir: directorio
      }
    });
    request.done(function(data){
      carpetaActual=directorio;
      listadoContenidos.html("");
      maquetarContenido(data);
    });
    request.fail(function(){});
    request.always(function(){});
  }

  function maquetarContenido(data){
    if(data.length>0){
      $.each(data, function(index, archivo){
        listadoContenidos.append(
          "<a data-tipo='"+archivo.tipo+"' data-url='"+archivo.url+"'>"+
            "<span>"+archivo.nombre+"</span>"+
            "<span>"+archivo.modificado+"</span>"+
            "<span>"+archivo.tamano+"</span>"+
            "<span>"+archivo.opciones+"</span>"+
          "</a>"
        );
      });
    }else{
      listadoContenidos.append("<div style='text-align: center;'>Carpeta vacía</div>");
    }
  }

  listadoContenidos.on("dblclick", "a", function(){
    archivo=$(this).data("url");
    tipo=$(this).data("tipo");

    if(tipo=="carpeta"){
      cargarContenidor(archivo);
    }
  });


  // *****************************
  // Renombrar (carpeta o archivo)
  // *****************************
  $(document).on("click", ".btnRenombrar", function(){
    var url=$(this).data("url");
    var arrayUrl=url.split("/");
    var indexArchivo=arrayUrl.length - 1;

    // Armo la URL sin el nombre del archivo/carpeta
    urlArray=url.split(arrayUrl[indexArchivo]);
    url=urlArray[0];

    $('#modale_renombrar').find("input[name='url']").val(url);
    $('#modale_renombrar').find("input[name='original']").val(arrayUrl[indexArchivo]);
    $('#modale_renombrar').find("input[name='nombre']").val(arrayUrl[indexArchivo]);
    $('#modale_renombrar').modal('show');
  });
  $(".renombrarAceptar").on("click", function(){
    url = $("#modale_renombrar").find("input[name='url']").val();
    original = $("#modale_renombrar").find("input[name='original']").val();
    nuevo = $("#modale_renombrar").find("input[name='nombre']").val();
    validacion = $("#modale_renombrar").find("input[name='nombre']").validationEngine('validate');

    if (!validacion && original != nuevo) {
      renombrar(url, original, nuevo);
    }
  });
  function renombrar(url, original, nuevo){
    var request=$.ajax({
      url: document.location.href,
      type: "POST",
      data: {
        accion: "renombrar",
        original: original,
        url: url,
        nuevo: nuevo
      }
    });
    request.done(function(data){
      $('#modale_renombrar').modal('hide');
      if(data.ok == true){
        cargarContenidor(url);
      }else{
        popupNotificacion(data.detalle, false);
      }
    });
    request.fail(function(){});
    request.always(function(){});
  }


  // *****************************
  // Eliminar (carpeta o archivo)
  // *****************************
  $(document).on("click", ".btnEliminar", function(){
    var url=$(this).data("url");
    var tipo=$(this).parent("span").parent("a").data("tipo");

    $('#modale_eliminar').find("input[name='url']").val(url);
    $('#modale_eliminar').find("input[name='tipo']").val(tipo);
    $('#modale_eliminar').modal('show');
  });
  $(".eliminarAceptar").on("click", function(){
    url=$("#modale_eliminar").find("input[name='url']").val();
    tipo=$("#modale_eliminar").find("input[name='tipo']").val();

    ultimoCorte=url.lastIndexOf("/");
    carpetaRaiz=url.substring(0, ultimoCorte);

    eliminar(url, tipo, carpetaRaiz);
  });
  function eliminar(url, tipo, carpetaRaiz){
    var request=$.ajax({
      url: document.location.href,
      type: "POST",
      data: {
        accion: "eliminar",
        url: url,
        tipo: tipo
      }
    });
    request.done(function(data){
      $('#modale_eliminar').modal('hide');
      if(data.ok == true){
        cargarDirectorios();
        cargarContenidor(carpetaRaiz);
      }else{
        popupNotificacion(data.detalle, false);
      }
    });
    request.fail(function(){
      popupNotificacion("Ocurrio un error al intetnra eliminar", false);
    });
    request.always(function(){});
  }


  // *****************************
  // Descargar (carpeta o archivo)
  // *****************************
  $(document).on("click", ".btnDescargar", function(){
    var url=$(this).data("url");
    var tipo=$(this).data("tipo");
    window.open(document.location.href+"?accion=descargar&archivo="+encodeURIComponent(url)+"&tipo="+tipo, "_blank");
  });


  // *****************************
  // PopUp Notificacion
  // *****************************
  function popupNotificacion(textoPopup, time, icon){
    var modal=$('#modal_popup');

    if(typeof time == 'undefined'){ time = 1000; }
    if(typeof icon == 'undefined'){ icon = false; }

    $('#modal_popup_texto').html("");
    if(icon !== false){
      $('#modal_popup_texto').html("<i class='fa fa-"+icon+" fa-lg'></i><br>");
    }
    $('#modal_popup_texto').append(textoPopup);
    modal.modal('show');
    if(time > 0){
      setTimeout(function(){
        modal.modal('hide');
      }, time);
    }
  }







  // Inicialización
  cargarDirectorios();
  cargarContenidor(dirRaiz);
});