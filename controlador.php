<?php
  error_reporting(E_ALL);
  session_start();

  // Configuración general
  $config=[
    "ver" => [
      "carpeta" => true,
      "archivo" => true
    ],
    "descargar" => [
      "carpeta" => false, // Esta funcion bien, pero crea un archivo ".zip" en el mismo directorio que el index.php y no deberia quedar ahí
      "archivo" => true
    ],
    "mover" => [
      "carpeta" => true,
      "archivo" => true
    ],
    "renombrar" => [
      "carpeta" => true,
      "archivo" => true
    ],
    "eliminar" => [
      "carpeta" => true,
      "archivo" => true
    ],
    "directorioRaiz" => "dirRaiz"
  ];

  // Controlador XHR
  if(isset($_REQUEST['accion'])){
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: GET, POST');
    header("Access-Control-Allow-Headers: X-Requested-With");
    header('Content-Type: application/json');
    switch($_REQUEST['accion']){
      case "cargarDirectorios":
        $resultado[]=[
          'nombre' => $config['directorioRaiz'],
          'href' => $config['directorioRaiz'],
          "cerrado" => false,
          "subcarpetas" => cargarDirectorios($config['directorioRaiz'])
        ];
        break;
      case "listarContenidoCarpeta":
        $resultado=listarContenidoCarpeta($_POST['dir']);
        break;
      case "descargar":
        decargar($_GET['archivo'], $_GET['tipo']);
        break;
      case "renombrar":
        $resultado=renombrar($_POST['url'], $_POST['original'], $_POST['nuevo']);
        break;
      case "moverCarpeta":
        break;
      case "moverArchivo":
        break;
      case "eliminar":
        $resultado=eliminar($_POST['url'], $_POST['tipo']);
        break;
      default:
        break;
    }
    echo json_encode($resultado);
    exit;
  }


  /********************************/
  /*** XHR ************************/
  /********************************/
  function cargarDirectorios($dir){
    global $config;
    $return=[];

    $dh=scandir($dir);
    foreach($dh as $folder){
      if($folder != '.' AND $folder != '..'){
        if(is_dir($dir.'/'.$folder)){
          $return[]=[
            'nombre' => $folder,
            'href' => $dir.'/'.$folder,
            "cerrado" => false,
            "subcarpetas" => cargarDirectorios($dir.'/'.$folder)
          ];
        }
      }
    }

    return $return;
  }

  function listarContenidoCarpeta($dir){
    global $config;

    $return=$returnDir=$returnFile=[];
    $dh=scandir($dir);
    foreach($dh as $folder){
      if($folder != '.' AND $folder != '..'){
        if(is_dir($dir.'/'.$folder)){
          $returnDir[]=[
            "nombre" => "<i class='fa fa-folder-o' aria-hidden='true'></i> ".$folder,
            "url" => $dir."/".$folder,
            "modificado" => "",
            "tamano" => "",
            "tipo" => "carpeta",
            "opciones" => listadoOpciones($dir."/".$folder, "carpeta")
          ];
        }else{
          $returnFile[]=[
            "nombre" => archivoIcono($folder)." ".$folder,
            "url" => $dir."/".$folder,
            "modificado" => date("d/m/Y H:i:s", filemtime($dir.'/'.$folder)),
            "tamano" => archivoTamano($dir."/".$folder),
            "tipo" => "archivo",
            "opciones" => listadoOpciones($dir."/".$folder, "archivo")
          ];
        }
      }
    }

    asort($returnDir);
    asort($returnFile);
    $return=array_merge($returnDir, $returnFile);

    return $return;
  }

  function renombrar($url, $original, $nuevo){
    global $config;
    $return=[];

    // Verificación de permisos
    if(
      (is_dir($url."/".$original) AND !$config['renombrar']['carpeta'])
      OR
      (is_file($url."/".$original) AND !$config['renombrar']['archivo'])
    ){
      $return["ok"]=false;
      $return["detalle"]="Accion no permitida (".$url."/".$original.")";
      return $return;
    }

    if(file_exists($url."/".$original)){
      if(rename($url."/".$original, $url."/".$nuevo)){
        $return["ok"]=true;
        $return["detalle"]="";
      }else{
        $return["ok"]=false;
        $return["detalle"]="Ocurrio un error al interntar renombrar";
      }
    }else{
      $return["ok"]=false;
      $return["detalle"]="El archivo o carpeta no existen (".$url."/".$original.")";
    }

    return $return;
  }

  function eliminar($url, $tipo){
    global $config;
    $return=[];

    // Verificación de permisos
    if(file_exists($url)){
      if(
        ($tipo=="carpeta" AND is_dir($url) AND !$config['eliminar']['carpeta'])
        OR
        ($tipo=="archivo" AND is_file($url) AND !$config['eliminar']['archivo'])
      ){
        $return["ok"]=false;
        $return["detalle"]="Accion no permitida (".$url.")";
        return $return;
      }
    }else{
      $return["ok"]=false;
      $return["detalle"]="El archivo o carpeta no existen (".$url.")";
      return $return;
    }

    if(is_dir($url) AND @rmdir($url)){
      $return["ok"]=true;
      $return["detalle"]="";
    }elseif(is_file($url) AND unlink($url)){
      $return["ok"]=true;
      $return["detalle"]="";
    }else{
      $return["ok"]=false;
      $return["detalle"]="Ocurrio un error al interntar eliminar";
      if($tipo=="carpeta"){
        $return["detalle"].="<br />Esto suele ocurrir cuando la carpeta no está vacío";
      }
    }

    return $return;
  }

  function decargar($archivo, $tipo){
    if($tipo=="archivo"){
      $archivoExplode=explode("/", $archivo);
      $nombreArchivo=$archivoExplode[(count($archivoExplode)-1)];

      header("Content-Type: application/download");
      header("Content-Disposition: attachment; filename=".$nombreArchivo);
      header("Content-Length: ".filesize($archivo));
      $fp=fopen($archivo, "r");
      fpassthru($fp);
      fclose($fp);
    }elseif($tipo=="carpeta"){
      $archivoExplode=explode("/", $archivo);
      $nombreArchivo=$archivoExplode[(count($archivoExplode)-1)];

      class FlxZipArchive extends ZipArchive {
        public function addDir($location, $name){
          $this->addEmptyDir($name);
          $this->addDirDo($location, $name);
        }
        private function addDirDo($location, $name){
          $name.='/';
          $location.='/';
          $dir=opendir($location);
          while($file=readdir($dir)){
            if($file=='.' || $file=='..') continue;
            $do=(filetype($location.$file) == 'dir') ? 'addDir' : 'addFile';
            $this->$do($location.$file, $name.$file);
          }
        }
      }

      $za=new FlxZipArchive;
      $res=$za->open($nombreArchivo.".zip", ZipArchive::CREATE);
      if($res===TRUE){
        $za->addDir($archivo, basename($archivo));
        $za->close();

        header('Content-Type: application/zip');
        header("Content-Disposition: attachment; filename=".$nombreArchivo.".zip");
        header('Content-Length: '.filesize($nombreArchivo.".zip"));
        header("Location: ".$nombreArchivo.".zip");
      }
    }
  }

  /********************************/
  /*** AREA CONTENIDO *************/
  /********************************/
  function listadoOpciones($url, $tipo){
    global $config;
    $html="";

    // Renombrar
    if($config["renombrar"][$tipo]){
      $html.="<button type='button' data-url='".$url."' class='btn btn-success btn-xs btnRenombrar'>";
      $html.="  <span class='glyphicon glyphicon-pencil' aria-hidden='true'></span>";
      $html.="</button> ";
    }

    // Descargar
    if($config["descargar"][$tipo]){
      $html.="<button type='button' data-url='".$url."' data-tipo='".$tipo."' class='btn btn-primary btn-xs btnDescargar'>";
      $html.="<span class='glyphicon glyphicon-download' aria-hidden='true'></span>";
      $html.="</button> ";
    }

    // Eliminar
    if($config["eliminar"][$tipo]){
      $html.="<button type='button' data-url='".$url."' class='btn btn-danger btn-xs btnEliminar'>";
      $html.="  <span class='glyphicon glyphicon-remove-circle' aria-hidden='true'></span>";
      $html.="</button> ";
    }

    return $html;
  }

  function archivoIcono($archivo){
    $archivoiExplode=explode(".", $archivo);

    switch($archivoiExplode[1]){
      case "pdf":
        $icono="<i class='fa fa-file-pdf-o' aria-hidden='true'></i>"; break;

      case "jpg":
      case "jpeg":
      case "gif":
      case "png":
        $icono="<i class='fa fa-file-picture-o' aria-hidden='true'></i>"; break;

      case "html":
      case "htm":
      case "php":
      case "js":
      case "css":
      case "bat":
        $icono="<i class='fa fa-file-code-o' aria-hidden='true'></i>"; break;

      case "doc":
      case "docx":
      case "dotx":
      case "rtf":
        $icono="<i class='fa fa-file-word-o' aria-hidden='true'></i>"; break;

      case "mp4":
      case "m4a":
      case "avi":
      case "mov":
        $icono="<i class='fa fa-file-video-o' aria-hidden='true'></i>"; break;

      case "xls":
      case "xlt":
      case "xls":
      case "xml":
        $icono="<i class='fa fa-file-excel-o' aria-hidden='true'></i>"; break;

      case "mp3":
      case "ogg":
        $icono="<i class='fa fa-file-audio-o' aria-hidden='true'></i>"; break;

      case "zip":
      case "rar":
      case "7z":
        $icono="<i class='fa fa-file-zip-o' aria-hidden='true'></i>"; break;

      case "txt":
        $icono="<i class='fa fa-file-text-o' aria-hidden='true'></i>"; break;

      default: $icono="<i class='fa fa-file' aria-hidden='true'></i>"; break;
    }

    return $icono;
  }

  function archivoTamano($archivo){
    $tamano=filesize($archivo)/1024;

    if($tamano < 900){
      $tamano=number_format($tamano, 1, ".", "")."KB";
    }else{
      $tamano=number_format(($tamano/1024), 1, ".", "")."MB";
    }

    return $tamano;
  }


  /********************************/
  /*** AREA DIRECTORIO ************/
  /********************************/
  function listarCarpetas($dir){
    $return=[];

    $dh=scandir($dir);
    foreach($dh as $folder){
      if($folder != '.' AND $folder != '..'){
        if(is_dir($dir.'/'.$folder)){
          $return[]=[
            'text' => $folder,
            'color' => "#555555",
            'backColor' => "#FFFFFF",
            'href' => $dir.'/'.$folder,
            'selectable' => true,
            'state' => [
              'checked' => false,
              'disabled' => false,
              'expanded' => false,
              'selected' => false
            ],
            "nodes" => listarCarpetas($dir.'/'.$folder)
          ];
        }
      }
    }
    return $return;
  }
  $listadoDirectorios=listarCarpetas($config['directorioRaiz']);